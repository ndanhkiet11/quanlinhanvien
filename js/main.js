var dsnv = [];

var dataJson = localStorage.getItem("DSNV");
if (dataJson) {
    var dataRaw = JSON.parse(dataJson);
    dsnv = dataRaw.map(function (nv) {
        return new NhanVien(
            nv.taiKhoan,
            nv.hoTen,
            nv.email,
            nv.matKhau,
            nv.ngayLam,
            nv.luongCoBan,
            nv.chucVu,
            nv.gioLam
        );
    });
    renderDsnv(dsnv);
}
function resetForm() {
    document.getElementById("form-id").reset();
}
// them nhan vien
function themNhanVien() {
    document.getElementById("header-title").style.display = "block";
    document.getElementById("header-title-2").style.display = "none";
    document.getElementById("btnThemNV").style.display = "block";
    document.getElementById("btnCapNhat").style.display = "none";
}
function themNv() {
    var newNv = layThongTinTuForm();

    document.getElementById("tbTKNV").style.display = "block";
    document.getElementById("tbTen").style.display = "block";
    document.getElementById("tbMatKhau").style.display = "block";
    document.getElementById("tbEmail").style.display = "block";
    document.getElementById("tbNgay").style.display = "block";
    document.getElementById("tbLuongCB").style.display = "block";
    document.getElementById("tbChucVu").style.display = "block";
    document.getElementById("tbGiolam").style.display = "block";

    var isValid = true;
    // kiem tra tai khoan
    isValid =
        isValid &
            kiemTraRong(newNv.taiKhoan, "tbTKNV") &
            kiemTraTaiKhoan(newNv.taiKhoan, "tbTKNV") &&
        kiemTraTrungTaiKhoan(newNv.taiKhoan, dsnv, "tbTKNV");
    // kiem tra ho ten
    isValid =
        isValid &
        kiemTraRong(newNv.hoTen, "tbTen") &
        kiemTraHoTen(newNv.hoTen, "tbTen");
    // kiem tra mật khẩu
    isValid =
        isValid &
        kiemTraRong(newNv.matKhau, "tbMatKhau") &
        kiemTraPassword(newNv.matKhau, "tbMatKhau");
    // kiểm tra email
    isValid =
        isValid &
        kiemTraRong(newNv.email, "tbEmail") &
        kiemTraEmail(newNv.email, "tbEmail");
    // kiểm tra ngày làm
    isValid =
        isValid &
        kiemTraRong(newNv.ngayLam, "tbLuongCB") &
        kiemTraNgayLam(newNv.ngayLam, "tbLuongCB");
    // kiểm tra lương
    isValid =
        isValid & kiemTraRong(newNv.luongCoBan, "tbLuongCB") &&
        kiemTraLuongCoBan(newNv.luongCoBan, "tbLuongCB");
    // kiểm tra chức vụ
    isValid = isValid & kiemTraChucVu(newNv.chucVu, "tbChucVu");
    // kiểm tra giờ làm
    isValid =
        isValid & kiemTraRong(newNv.gioLam, "tbGiolam") &&
        kiemTraGioLam(newNv.gioLam, "tbGiolam");

    if (isValid) {
        dsnv.push(newNv);
        renderDsnv(dsnv);
        saveLocalStorage();
        resetForm();
    }
}

function saveLocalStorage() {
    var dsnvJson = JSON.stringify(dsnv);
    localStorage.setItem("DSNV", dsnvJson);
}

// xoa nhan vien
function xoaNv(taiKhoan) {
    var index = dsnv.findIndex(function (nv) {
        return nv.taiKhoan == taiKhoan;
    });
    if (index == -1) return;

    dsnv.splice(index, 1);

    saveLocalStorage();
    renderDsnv(dsnv);
}

// cap nhat
function capNhat(taiKhoan) {
    document.getElementById("btnCapNhat").style.display = "block";
    document.getElementById("header-title").style.display = "none";
    document.getElementById("header-title-2").style.display = "block";
    document.getElementById("btnThemNV").style.display = "none";
    // disable input tài khoản
    document.getElementById("tknv").disabled = true;
    var index = dsnv.findIndex(function (nv) {
        return nv.taiKhoan == taiKhoan;
    });
    if (index == -1) return;

    var nv = dsnv[index];

    showThongTinLenForm(nv);
}

function capNhatNv() {
    var nvEdit = layThongTinTuForm();

    var index = dsnv.findIndex(function (nv) {
        return nv.taiKhoan == nvEdit.taiKhoan;
    });
    if (index == -1) return;

    document.getElementById("tbTKNV").style.display = "block";
    document.getElementById("tbTen").style.display = "block";
    document.getElementById("tbMatKhau").style.display = "block";
    document.getElementById("tbEmail").style.display = "block";
    document.getElementById("tbNgay").style.display = "block";
    document.getElementById("tbLuongCB").style.display = "block";
    document.getElementById("tbChucVu").style.display = "block";
    document.getElementById("tbGiolam").style.display = "block";

    var isValid = true;

    // kiem tra ho ten
    isValid =
        isValid &
        kiemTraRong(nvEdit.hoTen, "tbTen") &
        kiemTraHoTen(nvEdit.hoTen, "tbTen");
    // kiem tra mật khẩu
    isValid =
        isValid &
        kiemTraRong(nvEdit.matKhau, "tbMatKhau") &
        kiemTraPassword(nvEdit.matKhau, "tbMatKhau");
    // kiểm tra email
    isValid =
        isValid &
        kiemTraRong(nvEdit.email, "tbEmail") &
        kiemTraEmail(nvEdit.email, "tbEmail");
    // kiểm tra ngày làm
    isValid =
        isValid &
        kiemTraRong(nvEdit.ngayLam, "tbLuongCB") &
        kiemTraNgayLam(nvEdit.ngayLam, "tbLuongCB");
    // kiểm tra lương
    isValid =
        isValid & kiemTraRong(nvEdit.luongCoBan, "tbLuongCB") &&
        kiemTraLuongCoBan(nvEdit.luongCoBan, "tbLuongCB");
    // kiểm tra chức vụ
    isValid = isValid & kiemTraChucVu(nvEdit.chucVu, "tbChucVu");
    // kiểm tra giờ làm
    isValid =
        isValid & kiemTraRong(nvEdit.gioLam, "tbGiolam") &&
        kiemTraGioLam(nvEdit.gioLam, "tbGiolam");
    if (isValid) {
        dsnv[index] = nvEdit;
        console.log("dsnv[index]: ", dsnv[index]);
        saveLocalStorage();
        renderDsnv(dsnv);
    }
    // enable input tài khoản
    document.getElementById("tknv").disabled = false;
}

function search() {
    var loaiNv = document.getElementById("loai").value;

    var filterNv = dsnv.filter(function (nv) {
        return nv.xepLoai() == loaiNv;
    });
    renderDsnv(filterNv);
}
