function kiemTraRong(value, idError) {
    if (value.length == 0) {
        document.getElementById(idError).innerText =
            "Trường này không được để rỗng";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}

function kiemTraTaiKhoan(value, idError) {
    const re = /^[0-9]*$/;
    var isValid = re.test(value);
    if (!isValid) {
        document.getElementById(idError).innerHTML = "Tài khoản không hợp lệ";
        return false;
    } else if (value.length < 4 || value.length > 6) {
        document.getElementById(idError).innerHTML = "Tài khoản không hợp lệ";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}
function kiemTraTrungTaiKhoan(taiKhoan, listNv, idError) {
    var index = listNv.findIndex(function (nv) {
        return nv.taiKhoan == taiKhoan;
    });
    if (index == -1) {
        document.getElementById(idError).innerText = "";
        return true;
    } else {
        document.getElementById(idError).innerText = "Tài khoản đã tồn tại";
        return false;
    }
}
function kiemTraHoTen(value, idError) {
    const re =
        /^[a-zA-ZÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂẾưăạảấầẩẫậắằẳẵặẹẻẽềềểếỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ\s\W|_]+$/;
    var isValid = re.test(value);
    if (!isValid) {
        document.getElementById(idError).innerHTML = "Họ tên không hợp lệ";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}

function kiemTraPassword(value, idError) {
    var re =
        /^(?=.*[0-9])(?=.*[A-Z])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,10}$/;
    var isValid = re.test(value);
    if (!isValid) {
        document.getElementById(idError).innerHTML = "Mật khẩu không hợp lệ";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}

function kiemTraEmail(value, idError) {
    const re =
        /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;

    var isEmail = re.test(value);
    if (!isEmail) {
        document.getElementById(idError).innerText = "Email không hợp lệ";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}

function kiemTraNgayLam(value, idError) {
    const re = /^\d{1,2}\/\d{1,2}\/\d{4}$/;
    var isValid = re.test(value);
    if (!isValid) {
        document.getElementById(idError).innerText = "Ngày làm không hợp lệ";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}

function kiemTraLuongCoBan(value, idError) {
    var luongCB = value * 1;
    if (luongCB < 1000000 || luongCB > 20000000) {
        document.getElementById(idError).innerText =
            "Lương cơ bản không hợp lệ";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}

function kiemTraChucVu(value, idError) {
    if (value == 0) {
        document.getElementById(idError).innerText = "Vui lòng chọn chức vụ";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}

function kiemTraGioLam(value, idError) {
    var gioLam = value * 1;
    if (gioLam < 80 || gioLam > 200) {
        document.getElementById(idError).innerText = "Giờ làm không hợp lệ";
        return false;
    } else {
        document.getElementById(idError).innerText = "";
        return true;
    }
}
